//Add link to a picture from a fav movie to the array, remeber to put a comme after the previous link, no comme should be for the last link.
var img = [
    "https://gq-images.condecdn.net/image/qG0vGeYLb8R/crop/1020/f/Room-GQ-31Mar17_rex_b.jpg",
    "https://upload.wikimedia.org/wikipedia/en/3/3b/Pulp_Fiction_%281994%29_poster.jpg"  
]
var getImg = function(n, images){ 
    return images[n];
}
//Add a title for the movie, remeber to have the first letter uppercase and a dot after the title
var getTitle = function(n){
    var title = [
        "The room.",
        "Pulp Fiction."
    ]
    return title[n];
}
////////////////////////////////////////////////////////////
var makeCard = function(n) {
    var cards = `
        <div class="card">
            <span>${getTitle(n)}</span>
            <img src="${getImg(n, img)}">
        </div>
  `
    return cards;
}
var renderCard = function(n){
         document.querySelector("#cardContainer").innerHTML += makeCard(n);
}

var renderCards = function(){
    var i;
    for(i=0; i < img.length;i++){
        renderCard(i);
    }
}
renderCards();

this===window?null:module.exports = {
    getImg,
    getTitle,
    makeCard
};
